from django.forms import ModelForm
from recipes.models import Recipe

#class to create a form for recipe submissions
class RecipeForm(ModelForm):
    #info about the form
    class Meta:
        #RecipeForm takes the following fields
        #from model Recipe to create form
        model = Recipe
        fields = [
            "title",
            "picture",
            "description"
        ]
