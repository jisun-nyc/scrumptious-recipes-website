from django.shortcuts import render, get_object_or_404, redirect
from recipes.models import Recipe
from recipes.forms import RecipeForm
from django.contrib.auth.decorators import login_required


#That render function always takes the request object as its first argument.
#The second argument is the path to our HTML file that Django will send back to the browser.
#this form purpose is to get the data from the database,

def show_recipe(request, id):
    recipe = get_object_or_404(Recipe, id=id)
    context = {
        "recipe_object": recipe,
    }
    return render(request, "recipes/detail.html", context)



#this is a function to get all recipes
def recipe_list(request):
    recipes = Recipe.objects.all()
    context = {
        "recipe_list": recipes,
    }
    return render(request, "recipes/list.html", context)



#this is the function to view the recipe submission form
@login_required(login_url='/accounts/login/')
def create_recipe(request):
    if request.method == "POST":
        #we use the form to validate the values
        #and save them to the DB
        form = RecipeForm(request.POST)
        if form.is_valid():
            recipe = form.save(False)
            recipe.author = request.user
            recipe.save()
            #if all goes well, recipe is submitted and
            #client is rediected to recipe_list(homepage)
            return redirect("recipe_list")
    else:
        #create instance of django model form class
        form = RecipeForm()
    #put the form in the context
    context = {
        "form": form,
    }

    #render the HTML template with the form
    return render(request, "recipes/create.html", context)


#fx to edit existing recipe
def edit_recipe(request, id):
    recipe = get_object_or_404(Recipe, id=id)
    if request.method == "POST":
        form = RecipeForm(request.POST, instance=recipe)
        if form.is_valid():
            form.save()
            return redirect("show_recipe", id)
    else:
        #GET the recipeform for the requested recipe
        form = RecipeForm(instance=recipe)
    #
    context = {
        "form": form,
        "recipe_object": recipe,

    }
    return render(request, "recipes/edit.html", context)



#decorator, only logged in users can see this
@login_required
#function to view filtered recipes of your own
def my_recipe_list(request):
    recipes = Recipe.objects.filter(author=request.user)
    context = {
    "recipe_list": recipes,
    }
    return render(request, "recipes/list.html", context)
