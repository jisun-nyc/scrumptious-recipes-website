from accounts.views import signup, user_login, logout_view
from django.urls import path

urlpatterns = [
    path("signup/", signup, name="signup"),
    path("login/", user_login, name="login"),
    path("logout/", logout_view, name="logout"),
]
